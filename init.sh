#!/bin/bash

origin=$PWD
backup=$HOME/dotfiles.bak/$(date +%F_%H-%M-%S)
exclude=*.*\|WindowsPowerShell

for file in $origin/*
do
    name=$(basename $file)
    homepath=$HOME/.$name

    if [[ $name != @($exclude) ]]
    then
        echo "Creating link for .$name"

        # Backup existing dotfile
        if [[ ! -L "$homepath" && -e "$homepath" ]]
        then
            mkdir -p "$backup"
            mv "$homepath" "$backup/$name"
        fi

        # Create symlink
        ln -fs "$origin/$name" "$homepath"
    fi
done

# Pull in submodules
git submodule init && git submodule update

# Configure global gitignore
git config --global core.excludesfile $HOME/.gitignore_global

