# dotfiles
Priddles' config files for including, but not limited to:

- Bash shell
- Git SCM
- Neovim
- Windows PowerShell

Additionally contains:

- Handy initialisation scripts (`init.*`)
