let s:ConfigHome = expand('<sfile>:p:h') . '/config/'
let s:ConfigFiles = [
    \ 'init.vimrc',
    \ 'functions.vimrc',
    \ 'options.vimrc',
    \ 'general.vimrc',
    \ 'abbreviations.vimrc',
    \ 'mappings.vimrc',
    \ 'plugins.vimrc',
    \ ]

for file in s:ConfigFiles
    execute 'source ' . fnameescape(s:ConfigHome . file)
endfor
