" easymotion/vim-easyMotion
let g:EasyMotion_smartcase = 1
let g:EasyMotion_keys = 'aoeidtn'',.pyfgcrl/=;qjkxbmwvzuhs'

" vim-airline/vim-airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline_symbols.linenr = ''
let g:airline_symbols.maxlinenr = ''

" scrooloose/nerdtree
autocmd FileType nerdtree setlocal signcolumn=no

" junegunn/fzf.vim
let g:fzf_buffers_jump = 1
let g:fzf_commits_log_options = '--graph --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr"'
let g:fzf_tags_command = 'ctags -R'
let g:fzf_commands_expect = 'alt-enter,ctrl-x'

" ludovicchabant/vim-gutentags
let g:gutentags_exclude_filetypes = ['gitcommit', 'gitrebase', 'vim']
let g:gutentags_cache_dir = stdpath('data') . '/tags'
