" Copy/paste to/from clipboard
if has('win32')
    noremap <C-c> "*y
    noremap <C-v> "*p
    noremap <C-z> "*P
    inoremap <C-c> <C-o>"*y
    inoremap <C-v> <C-o>"*p
    inoremap <C-z> <C-o>"*P
endif

" Save and return to Normal mode
nnoremap <C-s> :up<CR>
inoremap <C-s> <Esc>:up<CR>

" Jump between windows
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Move by display lines, not actual lines
nnoremap j gj
nnoremap k gk

" Navigate completion menu
inoremap <expr> <C-b> pumvisible() ? '<C-p>' : '<C-b>'
" inoremap <expr> <CR> pumvisible() ? '<C-e><CR>' : '<CR>'
" inoremap <expr> <C-j> pumvisible() ? '<C-y><CR>' : '<CR>'
inoremap <expr> <Tab> pumvisible() ? coc#_select_confirm() : '<C-g>u<Tab>'

" Completion menu shortcuts
" inoremap <expr> . pumvisible() ? '<C-y>.' : '.'
" inoremap <expr> , pumvisible() ? '<C-y>,' : ','
" inoremap <expr> ( pumvisible() ? '<C-y>(' : '('
" inoremap <expr> ) pumvisible() ? '<C-y>)' : ')'
" inoremap <expr> < pumvisible() ? '<C-y><' : '<'
" inoremap <expr> > pumvisible() ? '<C-y>>' : '>'
" inoremap <expr> ; pumvisible() ? '<C-y>;' : ';'
" inoremap <expr> : pumvisible() ? '<C-y>:' : ':'
" inoremap <expr> -> pumvisible() ? '<C-y>->' : '->'

" Leader prefixed mappings
let mapleader = ','
nnoremap <Leader><Space> :nohlsearch<CR>

" airblade/vim-gitgutter
map ]u <Plug>GitGutterNextHunk
map [u <Plug>GitGutterPrevHunk

" easymotion/vim-easyMotion
map / <Plug>(easymotion-sn)
map n <Plug>(easymotion-next)
map N <Plug>(easymotion-prev)
map <Plug>(easymotion-prefix)s <Plug>(easymotion-overwin-f2)

" junegunn/fzf.vim
nmap <Leader><Tab> <Plug>(fzf-maps-n)
xmap <Leader><Tab> <Plug>(fzf-maps-x)
omap <Leader><Tab> <Plug>(fzf-maps-o)
nnoremap <Leader>ss :Ag<CR>
nnoremap <Leader>sg :GFiles<CR>
nnoremap <Leader>sG :GFiles?<CR>
nnoremap <Leader>sc :BCommits<CR>
nnoremap <Leader>sC :Commits<CR>
nnoremap <Leader>sf :Files<CR>
nnoremap <Leader>sb :Buffers<CR>
nnoremap <Leader>sl :BLines<CR>
nnoremap <Leader>sL :Lines<CR>
nnoremap <Leader>st :BTags<CR>
nnoremap <Leader>sT :Tags<CR>

" scrooloose/nerdtree
noremap <silent> <C-n> :NERDTreeFocus<CR>

" SirVer/ultisnips
let g:UltiSnipsExpandTrigger =       '<M-n>'
let g:UltiSnipsJumpForwardTrigger =  '<M-n>'
let g:UltiSnipsJumpBackwardTrigger = '<M-b>'
