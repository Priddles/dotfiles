function FindProjectRoot(names) abort
    let names = a:names + ['.git', '.root']

    for name in names
        let relativePath = finddir(name, expand('%:p:h') . '.;')

        if !empty(relativePath)
            return fnamemodify(relativePath, ':p:h:h')
        endif
    endfor

    return ''
endfunction
