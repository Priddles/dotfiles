set background=dark
set completeopt=noinsert,menuone,preview
set eol fixeol
set expandtab
set fileformat=unix fileformats=unix,dos
set gdefault
set hidden
set ignorecase smartcase
set list listchars+=extends:>,precedes:<
set matchpairs+=<:>
set nohlsearch
set noshowmode
set number
set scrolloff=4 sidescrolloff=5
set showmatch
set signcolumn=yes
set smartindent shiftwidth=4 tabstop=4 softtabstop=4
set statusline=2
set textwidth=120 colorcolumn=+1
set updatetime=250
set visualbell

set wildignore+=*.7z,*.gz,*.iso,*.jar,*.rar,*.tar,*.zip
set wildignore+=*.o,*.class,*.dll,*.exe
set wildignore+=*/tmp/*,*/temp/*,*/vendor/*,*/bin/*,*/build/*,*/.git/*,*/venv/*

if has('termguicolors') && empty($TMUX)
    set termguicolors
endif

if has('win32')
    " XXX: PowerShell disabled because the fzf.vim plugin is hard-coded to use cmd.exe
    " set shell=powershell shellpipe=\| shellquote= shellxquote="
    " set shellredir=\|Out-File\ -Encoding\ ASCII\ -FilePath\ %s
    " set shellcmdflag=-NoLogo\ -ExecutionPolicy\ RemoteSigned\ -Command
endif
