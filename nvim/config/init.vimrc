" Global constants
let g:PowerShell = 'PowerShell -NonInteractive -NoLogo -NoProfile -ExecutionPolicy RemoteSigned '

" Load plugins
let s:plug_home = stdpath('config') . '/plugged'
call plug#begin(s:plug_home)

" UI
Plug 'junegunn/fzf' | Plug 'junegunn/fzf.vim'
Plug 'scrooloose/nerdtree' | Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'sheerun/vim-polyglot'
Plug 'vim-airline/vim-airline'
Plug 'ryanoasis/vim-devicons'

" Colorschemes
Plug 'morhetz/gruvbox'
Plug 'rakr/vim-one'

" Editing
Plug 'easymotion/vim-easymotion'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'

" Autocomplete
Plug 'SirVer/ultisnips'
" Plug 'ludovicchabant/vim-gutentags'

Plug 'neoclide/coc.nvim', {'branch': 'release'}
" CoC managed plugins:
"  - coc-extensions/coc-powershell
"  - iamcco/coc-vimlsp
"  - neoclide/coc-git
"  - neoclide/coc-java
"  - neoclide/coc-syntax
"  - neoclide/coc-tag
"  - neoclide/coc-ultisnips

call plug#end()
