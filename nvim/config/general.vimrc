" Disable unused providers
let g:loaded_python_provider = 1
let g:loaded_ruby_provider = 1
let g:loaded_perl_provider = 1

" Python provider
let s:python3_home = stdpath('config') . '\py3env\Scripts\python'
if filereadable(s:python3_home . '.exe')
    let g:python3_host_prog = s:python3_home
endif

" Appearance
colorscheme one
