# If not running as administrator, exit.
$CurrentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
$IsAdmin = [bool]($CurrentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))

if (!$IsAdmin)
{
    Write-Error -Category PermissionDenied -Message 'Administrator privileges are required to run this script.'
    exit
}

Set-Location $PSScriptRoot



# Constants
$OriginRoot = $PSScriptRoot
$TargetRoot = $HOME
$BackupDir = "$HOME\.config.bak\$(Get-Date -Format 'yyyy-MM-dd_HH-mm-ss')"

$XDG_CONFIG_HOME = $OriginRoot
$XDG_DATA_HOME = "$HOME\AppData\Local"
$XDG_CACHE_HOME = "$HOME\AppData\Local\Temp"

$PSModulesDir = "$OriginRoot\powershell\modules"
$GitLocalConfig = "$HOME\.gitconfig"
$ConfigModuleName = 'Config'



# Set user environment variables
$EnvVars = @{
    'XDG_CONFIG_HOME' = $XDG_CONFIG_HOME
    'XDG_DATA_HOME'   = $XDG_DATA_HOME
    'XDG_CACHE_HOME'  = $XDG_CACHE_HOME
    'LESSHISTFILE'    = "$XDG_DATA_HOME\less\lesshst"
}

Write-Host 'Setting user environment variables...'
foreach ($variable in $EnvVars.Keys)
{
    $value = $envVars.$variable

    Write-Host "  $variable => $value"
    [System.Environment]::SetEnvironmentVariable($variable, $value, 'User')
}

if ($EnvVars.Count -eq 0) { Write-Host 'Nothing to do.' }
else { Write-Host 'Done.' }
Write-Host



# Map paths relative to $OriginRoot to targets relative to $TargetRoot
$ToLink = @{
    #'vim\vimrc' = '_vimrc'
    #'hyper\hyper.js' = '.hyper.js'
    'windows-terminal\settings.json' = "AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json"
    'vscode\settings.json' = 'scoop\persist\vscode\data\user-data\User\settings.json'
}

Write-Host 'Creating symbolic links...'
foreach ($origin in $ToLink.Keys)
{
    $originPath = Join-Path $OriginRoot $origin
    $targetPath = Join-Path $TargetRoot $ToLink.$origin

    if (Test-Path $targetPath)
    {
        $targetItem = Get-Item $targetPath
        $isSymbolicLink = $targetItem.Attributes.HasFlag([System.IO.FileAttributes]::ReparsePoint)

        if (!$isSymbolicLink)
        {
            Write-Host "  '$targetPath' already exists! Backing up."

            New-Item -Path $BackupDir -ItemType Directory -Force | Out-Null
            Move-Item -Path $targetPath -Destination $BackupDir -Force
        }
    }

    Write-Host "  $targetPath => $originPath"
    New-Item -ItemType SymbolicLink -Path $targetPath -Value $originPath -Force | Out-Null
}

if ($ToLink.Count -eq 0) { Write-Host 'Nothing to do.' }
else { Write-Host 'Done.' }
Write-Host



# Configure powershell
Write-Host 'Configuring PowerShell...'
if (!$env:PSModulePath.Contains($PSModulesDir))
{
    Write-Host '  Updating $PSModulePath...'
    [System.Environment]::SetEnvironmentVariable('PSModulePath', "$PSModulesDir;$env:PSModulePath", 'User')
}

if (!(Select-String "(Import-Module|ipmo) $ConfigModuleName" $PROFILE))
{
    Write-Host '  Updating $PROFILE...'
    Add-Content $PROFILE "Import-Module $ConfigModuleName"
}
Write-Host "Done.`n"



# Configure git
Write-Host 'Configuring git...'
if (!$(git config --file $GitLocalConfig --get core.editor))
{
    git config --file $GitLocalConfig core.editor "code --wait"
}

if (!$(git config --file $GitLocalConfig --get credential.helper))
{
    git config --file $GitLocalConfig credential.helper "manager"
}


# Pull in submodules
Write-Host 'Loading git submodules...'
git submodule init; git submodule update



# Final message
Write-Host "`nAll done! Restart the console to see changes."
if (Test-Path $BackupDir)
{
    Write-Host " Any backed up files can be found at: $BackupDir"
}
