New-Alias cout Get-ChocoOutdated
New-Alias dig Resolve-DnsName
New-Alias dupe Start-Powershell
New-Alias gw Invoke-GradleWrapper
New-Alias n nvim
New-Alias nq nvim-qt
New-Alias sout Get-ScoopStatus
New-Alias which Get-CommandDescription
New-Alias zz Open-FzFile

Export-ModuleMember -Alias *
