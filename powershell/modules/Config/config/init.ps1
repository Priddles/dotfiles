foreach ($Alias in @("diff", "curl"))
{
    if (Test-Path "Alias:\$Alias")
    {
        Remove-Item -Force "Alias:\$Alias"
    }
}
