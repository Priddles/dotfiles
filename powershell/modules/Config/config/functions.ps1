function Get-ChocoOutdated { choco outdated }
function Get-ScoopStatus { scoop update; scoop status }
function Start-Powershell { Start-Process PowerShell }
function Get-CommandDescription($name) { Get-Command $name | Select-Object -ExpandProperty Definition }

function Ping-Finity($target) {
    if (!$target) { $target = 'spintel.com.au' }
    while ($true) {
        ping -t $target
    }
}

function Open-FzFile {
    [CmdletBinding()]

    Param (
        [alias('m')]
        [switch]$UseMultiSelect
    )

    Begin {
        $Flags = @()
        if ($UseMultiSelect) {
            $Flags += '-m'
        }

        $FilesToOpen = fzf $Flags
    }

    Process {
        foreach ($File in $FilesToOpen) {
            & ".\$File"
        }
    }
}

function Find-ProjectRoot {
    [CmdletBinding()]

    param (
        [parameter(ValueFromPipeline)]
        [alias('p')]
        [string]$Path = $(Get-Location),

        [alias('rm', 'm')]
        [string[]]$RootMarkers,

        [switch]$SkipDefaultMarkers
    )

    Begin
    {
        $DefaultRootMarkers = @(
            # VCS
            '.git', '.hg',

            # Gradle
            'gradlew', 'gradlew.bat',

            # Defaults
            'src', '.root'
        )
    }

    Process
    {
        if (!$SkipDefaultMarkers)
        {
            $RootMarkers += $DefaultRootMarkers
        }

        Write-Verbose "Attempting to find project markers along path: $Path"
        while ($Path)
        {
            foreach ($Marker in $RootMarkers)
            {
                $PathToTest = Join-Path $Path $Marker

                Write-Debug "Test path: $PathToTest"
                if (Test-Path $PathToTest)
                {
                    return Get-Item $Path
                }
            }

            $Path = Split-Path $Path -Parent
        }

        return $NULL
    }
}

function Invoke-GradleWrapper
{
    [CmdletBinding(
        PositionalBinding=$False,
        SupportsShouldProcess)]

    Param
    (
        [parameter(
            Position=0,
            ValueFromRemainingArguments)]
        [alias('args')]
        [string[]] $ArgumentList,

        [parameter(ValueFromPipeline)]
        [alias('p')]
        [string]$Path,

        [alias('d')]
        [string]$ProjectDir
    )

    Process
    {
        $WrapperName = 'gradlew.bat'
        $WrapperPath = Find-ProjectRoot $Path ($WrapperName) -SkipDefaultMarkers

        if (!$WrapperPath)
        {
            $ErrorMessage = "Could not find Gradle wrapper along path: $Path"
            $ErrorId = 'GradleWrapperNotFound'
            Write-Error -Category ObjectNotFound -ErrorId $ErrorId -Message $ErrorMessage -TargetObject $Path
            return
        }

        $Wrapper = Join-Path $WrapperPath $WrapperName
        if (!$ProjectDir) { $ProjectDir = $WrapperPath }

        # ShouldProcess description
        $InvokeWrapperDescription = "Invoking Gradle wrapper located at `"$Wrapper`" with "
        if ($ArgumentList) { $InvokeWrapperDescription += "arguments `"$ArgumentList`"" }
        else { $InvokeWrapperDescription += "no arguments" }
        $InvokeWrapperDescription += " using project directory `"$ProjectDir`"."

        if ($PSCmdlet.ShouldProcess($InvokeWrapperDescription, $NULL, $NULL))
        {
            & $Wrapper '--project-dir' $ProjectDir $ArgumentList
        }
    }
}

function Expand-Archive {
    [CmdletBinding(SupportsShouldProcess)]

    Param
    (
        [parameter(
            Mandatory,
            ValueFromPipeline)]
        [alias('p')]
        [string] $Path,

        [alias('d')]
        [string] $Destination = $(Get-Location),

        [alias('n')]
        [string] $Name
    )

    Process
    {
        $Archive = Get-Item $Path

        if (!$Archive) { return }

        $Name_ = $Name
        if (!$Name_) { $Name_ = $Archive.BaseName }

        $Destination_ = Join-Path $Destination $Name_

        # ShouldProcess target
        $ExpandTarget = "Path: $Archive Destination\Name: $Destination_"

        if ($PSCmdlet.ShouldProcess($ExpandTarget))
        {
            7z x -spe "-o$Destination_" $Archive
        }
    }
}

function Read-ArchiveRoot {
    [CmdletBinding()]

    Param
    (
        [parameter(
            Mandatory,
            ValueFromPipeline)]
        [alias('p')]
        [string] $Path,

        [alias('d')]
        [int] $Depth = 1
    )

    Process
    {
        $Archive = Get-Item $Path

        if (!$Archive) { return }

        $Exclude = '*\*'
        for ($i = 1; $i -lt $Depth; $i++)
        {
            $Exclude += '\*'
        }

        7z l "-x!$Exclude" $Archive
    }
}

function Connect-PuttySshSession {
    [CmdletBinding()]

    Param
    (
        [parameter(
            Position = 0,
            Mandatory,
            ParameterSetName='Profile')]
        [alias('p')]
        [string] $Profile,

        [parameter(
            Mandatory,
            ParameterSetName='Target')]
        [alias('t')]
        [string] $Target
    )

    Begin
    {
        $Profiles = @{
            'cse' = 'z3424661@login.cse.unsw.edu.au';
            'cse-grieg' = 'z3424661@grieg.cse.unsw.edu.au'
        }
    }

    End
    {
        if ($Profile)
        {
            if (!$Profiles.ContainsKey($Profile))
            {
                $ErrorMessage = "Unknown profile: $Profile"
                $ErrorId = 'UnknownProfile'
                Write-Error -Category InvalidArgument -ErrorId $ErrorId -Message $ErrorMessage -TargetObject $Path
                return
            }

            $Target = $Profiles[$Profile]
        }

        putty -ssh $Target
    }
}

Export-ModuleMember -Function *
