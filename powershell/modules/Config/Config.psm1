. "$PSScriptRoot\config\aliases.ps1"
. "$PSScriptRoot\config\functions.ps1"

Update-TypeData -PrependPath "$PSScriptRoot\resources\FileSize.ps1xml"
Update-FormatData -PrependPath "$PSScriptRoot\resources\FileSize.format.ps1xml"
